import firebase from "firebase/app";
import "firebase/analytics";
import "firebase/auth";
import "firebase/database"

const firebaseConfig = {
    apiKey: "AIzaSyCELLtX3MXl3jFoAcOXiTMAymXmDWqiWQg",
    authDomain: "wu-website-c2822.firebaseapp.com",
    projectId: "wu-website-c2822",
    storageBucket: "wu-website-c2822.appspot.com",
    messagingSenderId: "601198480745",
    appId: "1:601198480745:web:6a7b9629a6c46c695c81f8",
    measurementId: "G-4BFX98Y3WD"
};

firebase.initializeApp(firebaseConfig);

export default firebase;