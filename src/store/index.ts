import { createStore } from "vuex"
import firebase from "@/utilities/firebase";

export default createStore({
  state: {
    isAuthenticate: false,
  },
  mutations: {
    authenticateCheck(state, payload) {
      state.isAuthenticate = payload
    }
  },
  actions: {
    setAuth({ commit }) {
      // const db = firebase.database();
      
			firebase.auth().onAuthStateChanged((user) => {
				if (user) {
          // User is signed in.
          commit('authenticateCheck', true)
					// console.log('Logged in.');
				} else {
          // No user is signed in.
          commit('authenticateCheck', false)
					// console.log("Not login.");
				}
			});
		},
  },
  modules: {

  }
})