import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import Learning from '../views/Learning.vue'
import Calander from '../views/Calander.vue'
import Stock from "../views/Stock.vue"

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/learning',
    name: 'Learning',
    component: Learning
  },
  {
    path: '/calander',
    name: 'Calander',
    component: Calander
  },
  {
    path: '/stock',
    name: 'Stock',
    component: Stock
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
